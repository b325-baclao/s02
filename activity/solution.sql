/*
	ACTIVITY:

		1. Create a blog_db database
		2. Write the SQL Code necessary to create the shown tables and images

			Note: Also include the primary key and foreign key constraints in your answer.

    Comment:
    Foreign key constraints were added to establish relationships between the tables.

    ON DELETE CASCADE option to ensure that when a referenced row is deleted from the users or posts table, the corresponding rows in related tables (post_likes and post_comments) will also be automatically deleted. This helps maintain data integrity and consistency across the database.
*/


CREATE DATABASE blog_db;

-- Table for users
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    INDEX idx_username (username)
);

-- Table for posts
CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    -- Index for efficient querying by author ID.
    INDEX idx_author_id (author_id),
    -- Index for sorting and filtering by posting date.
    INDEX idx_datetime_posted (datetime_posted),
    CONSTRAINT fk_posts_author_id
        FOREIGN KEY (author_id) REFERENCES author(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


-- Table for posts likes
CREATE TABLE post_likes (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
    INDEX idx_post_id (post_id),
    INDEX idx_user_id (user_id),
    INDEX idx_datetime_liked (datetime_liked),
    CONSTRAINT fk_post_likes_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_post_likes_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

-- Table for posts comments
CREATE TABLE post_comments (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY (id),
    INDEX idx_post_id (post_id),
    INDEX idx_user_id (user_id),
    INDEX idx_datetime_commented (datetime_commented),
    CONSTRAINT fk_post_comments_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_post_comments_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);


