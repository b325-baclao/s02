-- SQL syntaxes are not case sensitive.
-- If you are going to add a SQL syntax(capitalize).
-- If tou are going to use or add name of the column or table(small letter).

-- Installing MariaDB for MacOS: https://www.youtube.com/watch?v=bY8JTC7VTvY
-- Data Types in MySQL: Tutorial and Full List with Examples of Data Formats: https://blog.devart.com/mysql-data-types.html

-- To show all the list of our databases:
SHOW DATABASES;

-- Create a database.
CREATE DATABASE music_db;
-- Drop database.
DROP DATABASE music_db;
-- Recreate the database.
CREATE DATABASE music_db;
-- Select a database.
USE music_db;
-- Show table inside a database:
SHOW TABLES;
-- Rename a table:
ALTER TABLE table_to_be_edited RENAME updated_name;
RENAME TABLE table_to_be_edited TO updated_name;

-- Creating a table:
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,2
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);


-- MINI ACTIVITY: Create the artist's table
		-- name (not nullable)

CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
        FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


-- Table for songs: not nullable
CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    duration TIME NOT NULL,
    genre VARCHAR(50),
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY (album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_playlists_users_id
        FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


-- Multiple Foreign Key in one table:
-- This table stores the associations between playlists and songs.
-- Each row represents a song included in a specific playlist.

CREATE TABLE playlists_songs (
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY(id),
    -- This constraint ensures that playlist_id references the playlists table.
    CONSTRAINT fk_playlists_songs_playlist_id
        FOREIGN KEY(playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    -- This constraint ensures that song_id references the songs table.
    CONSTRAINT fk_playlists_songs_song_id
        FOREIGN KEY(song_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);























